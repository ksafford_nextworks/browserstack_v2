package com.nextworks.selenium_tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;

public class AddTabs extends NWActivity{

	static String[]  newTabNames = {
		"Main Page",
		"Tab1",
		"Tab2",
		"Tab3"
	};
	
	static String[] defaultTabNames = {
		"Home",
		"Page",
		"Page",
		"Page"
	};
	
	static String[] tabsNamesXpathes = {
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li/a/tab-heading/div/div/span",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[2]/a/tab-heading/div/div/span",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[3]/a/tab-heading/div/div/span",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[4]/a/tab-heading/div/div/span"
	};
	
	static String[] tabsXpathes = {
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[1]/a",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[2]/a",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[3]/a",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[4]/a"
	};
	
	static String[] inputFieldsXpathes = {
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[1]/a/tab-heading/form/input",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[2]/a/tab-heading/form/input",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[3]/a/tab-heading/form/input",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[4]/a/tab-heading/form/input"
	};
	
	static String[] okButtonsXpathes = {
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[1]/a/tab-heading/form/div/button",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[2]/a/tab-heading/form/div/button",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[3]/a/tab-heading/form/div/button",
		"/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/ul/li[4]/a/tab-heading/form/div/button"
	};
	
	public static void addNewTab() {
		WebElement addTabButton = driver.findElement(By.cssSelector("button[ng-click='vm.addPage()']"));
		for (int i=0; i<newTabNames.length-1; i++) {
			addTabButton.click();
		}
	}
	
	public static void assertTabsAdded() {
		List<WebElement> allTabs = driver.findElements(By.className("tab-title"));
		assertEquals(newTabNames.length, allTabs.size());
	}
	
	public static void renameTabs(String defaultTabName, String tabNameXpath, String inputFieldXpath, String newName, String smallOkButtonXpath) {
		//WebElement tab = driver.findElement(By.xpath(tabXpath));
		WebElement tab = driver.findElement(By.linkText(defaultTabName));
		tab.click();
		
		WebElement tabName = driver.findElement(By.xpath(tabNameXpath));
		//WebElement tabName = driver.findElement(By.xpath("//span[contains(., 'Page')]"));
		tabName.click();
		
		WebElement inputField = driver.findElement(By.xpath(inputFieldXpath));
		inputField.clear();
		inputField.sendKeys(newName);
		
		WebElement okButton = driver.findElement(By.xpath(smallOkButtonXpath));
		okButton.click();
	}
	
	public static void assertRename(String tabNameXpath, String expectedTabName) {
		WebElement tab = driver.findElement(By.xpath(tabNameXpath));
		String tabName =tab.getText();
		assertEquals(tabName, expectedTabName);
	}
}
