package com.nextworks.selenium_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class AddTextWidget extends NWActivity{

	static String widgetName = "Text";
	static String categoryName = "Rich Content"; 
	static String textWidgetToDragXpath = "/html/body/div/div[3]/div/div/div/div[2]/div/div/div/div/div[1]/div[2]/ul/li";
	static String targetForTextWidgetXpath = "/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/div/div/assign-widgets-to-regions/div/div/div/div[2]/div[1]/div/div/div/div/div";
	static String textInTheEditorContainerXpath = ".//*[@id='assign-widgets-to-regions']/div/div/div[2]/div[1]/div/div";
	static String xpathForAssertWidgetDeletedFromEditorMethod = ".//*[@id='assign-widgets-to-regions']/div/div/div[2]/div[1]/div/div/div/div[1]/a[1]//*"; //xpath of container + "//*"
	
	
	static String title = "Test title";
	static String description = "Test description";
	static String text = "In vino veritas, in aqua sanitas";
	
	public static void addTextWidget() {
		addWidget(widgetName, categoryName, title, description);
		
		WebElement textField = driver.findElement(By.cssSelector("div[ta-bind='ta-bind']"));
		textField.clear();
		textField.sendKeys(text);
		
		clickOkWidgetButton();
	}

	public static void assertWidgetIsAdded() {
		WebElement addedWidget = driver.findElement(By.name(title));
		String addedTextWidgetName = addedWidget.getText();
		assertEquals(addedTextWidgetName, title);
		
		/*
		WebElement textWidgetSettings = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div/div[3]/div/div/div/div[2]/div/div/div/div[1]/div[2]/ul/li/a/i"));
		textWidgetSettings.click();
		
		WebElement titleField = driver.findElement(By.id("title"));
		String actualTitle = titleField.getAttribute("value");
		assertEquals(actualTitle, title);
		
		WebElement descriptionField = driver.findElement(By.id("description"));
		String actualDescription = descriptionField.getAttribute("value");
		assertEquals(actualDescription, description);
		
		WebElement textField = driver.findElement(By.cssSelector("div[ta-bind='ta-bind']"));
		String actualText = textField.getText();
		assertEquals(actualText, text);
		
		WebElement cancelButton = driver.findElement(By.cssSelector("button[data-ng-click='cancel()']"));
		cancelButton.click();
		*/
	}
		
	public static void assertTextDragged() {
		//WebElement target = driver.findElement(By.xpath(targetForTextWidgetXpath));
		WebElement target = driver.findElement(By.cssSelector("div[data-region-id='2']"));
		String draggedText = target.getText();
		assertEquals(draggedText, text);
	}
	
	public static void assertTextIsNoLongerPresent() {
		assertFalse(driver.getPageSource().contains(text));
	}
	
	
}
