package com.nextworks.selenium_tests;

import static org.junit.Assert.assertEquals;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateCapsule extends NWActivity{	
	static String agencyName = "Test";
	static String clientName = "Selenium";
	
	static String submitButtonXpath = "/html/body/div/div[3]/div/div/div/div[1]/div[1]/form/div/button"; //save new capsule name button
	static String createCapsuleXpath = "/html/body/div/div[2]/div/div/div/nav/ul/li[1]/a/b/i";
	static String recentCapsuleXpath = "/html/body/div/div[2]/div/div/div/nav/ul/li[2]/a/span[2]"; //first capsule in the list of all client's capsules
	static String publishingTabXpath = "/html/body/div/div[3]/div/div/div/div[2]/div/div/div/ul/li[3]/a";
	static String saveCapsuleButtonXpath = "/html/body/div/div[3]/div/div/div/div[2]/div/div/div/div/div[3]/div/div[1]/form/button[1]";
	
	static String defaultCapsuleName = "New Capsule";
	static Random rn = new Random();
	static String newCapsuleName = "Test Capsule "+rn.nextInt(10000);
	
	static String defaultCapsuleDescription = "Type in a description of your capsule"; 
	static String newCapsuleDescription = "O tempora, о mores!";
	
	static String capsuleName;
	static String capsuleIdentifyer;
	static String capsuleID;
	
	public static void openAgency() {
		WebElement agency = driver.findElement(By.name(agencyName));
		agency.click();
	}
	
	public static void openClient() {
		WebElement client = driver.findElement(By.name(clientName)); 
		client.click();
	}
	
	public static void createNewCapsule() {
		openAgency();
		openClient();
		
		WebElement createCapsule = driver.findElement(By.className("label"));
		
		Actions action = new Actions(driver);
		action.moveToElement(createCapsule).click().build().perform();
		
		//waitForElementPresent(capsuleNameTextFieldXpath);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h1[ng-click='edittingTitle = !edittingTitle']")));
		
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.findElement(By.tagName("h1")).getText().length()!=0;
			}
		});
		
		WebElement capsuleNameTextField = driver.findElement(By.tagName("h1"));
		String createdCapsuleName = capsuleNameTextField.getText();
		assertEquals(createdCapsuleName, defaultCapsuleName);
	}
	
	public static void renameCapsule() {
		WebElement capsuleNameTextField = driver.findElement(By.tagName("h1"));
		capsuleNameTextField.click();
		
		WebElement capsuleNameTextEdit = driver.findElement(By.cssSelector("input[ng-model='settings.title']"));
		capsuleNameTextEdit.click();
		capsuleNameTextEdit.clear();
		capsuleNameTextEdit.sendKeys(newCapsuleName);
		
		WebElement submitButton = driver.findElement(By.cssSelector("button[class='btn btn-default btn-xs']"));
		submitButton.click();
		
		WebElement capsuleNameTextField2 = driver.findElement(By.tagName("h1"));
		String renamedCapsule = capsuleNameTextField2.getText();
		assertEquals(renamedCapsule, newCapsuleName);
	}
	
	public static String getCapsuleName() {
		WebElement capsuleNameBox = driver.findElement(By.tagName("h1"));
		String currentCapsuleName = capsuleNameBox.getText();
		return currentCapsuleName;
	}
	
	public static String getCapsuleId() {
		String newCapsuleUrl = driver.getCurrentUrl();
		String capsuleIdentifyer = newCapsuleUrl.substring(51, newCapsuleUrl.length()-5);
		return capsuleIdentifyer;
	}
	
	public static void addDescription() {
		WebElement capsuleNameTextField = driver.findElement(By.tagName("h1"));
		capsuleNameTextField.click();
		
		WebElement descriptionTextEdit = driver.findElement(By.cssSelector("input[ng-model='settings.description']"));
		descriptionTextEdit.click();
		descriptionTextEdit.clear();
		descriptionTextEdit.sendKeys(newCapsuleDescription);
		
		WebElement submitButton = driver.findElement(By.cssSelector("button[class='btn btn-default btn-xs']"));
		submitButton.click();
		
		WebElement capsuleDescription = driver.findElement(By.cssSelector("small[ng-click='edittingTitle = !edittingTitle']"));
		String newDescription = capsuleDescription.getText();
		assertEquals(newDescription, newCapsuleDescription);
	}
	
	public static void waitForUrlToChangeUponClick(String elementXpath, String expectedURL) {
		String previousURL = driver.getCurrentUrl();
		String currentURL = null;
		WebDriverWait wait = new WebDriverWait(driver, 10);

		WebElement buttonToClick = driver.findElement(By.xpath(elementXpath)); 
		buttonToClick.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		ExpectedCondition e = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return (d.getCurrentUrl() != previousURL);
		    }
		};

		wait.until(e);
		currentURL = driver.getCurrentUrl();
		//System.out.println(currentURL);
		assertEquals(currentURL, expectedURL); 
	}
	
	public static void saveCapsule() {
		WebElement saveButton = driver.findElement(By.cssSelector("button[ng-click='save();']"));
		saveButton.click();
	}
	
	public static void checkCapsuleWasSaved(String nameAttribute, String expectedCapsuleName) {
		driver.get(getContentURL());
		openAgency();
		openClient();
		
		WebElement recentCapsule = driver.findElement(By.name(nameAttribute));
		recentCapsule.click();
		
		WebElement editButton = driver.findElement(By.linkText("Edit"));
		editButton.click();
		
		String currentCapsuleId = getCapsuleId();
		assertEquals(currentCapsuleId, nameAttribute);
		
		WebElement capsuleNameField = driver.findElement(By.tagName("h1"));
		String capsuleName = capsuleNameField.getText();
		
		assertEquals(capsuleName, expectedCapsuleName);
		
	}
}
