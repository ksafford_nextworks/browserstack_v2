package com.nextworks.selenium_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;

public class DeleteCapsule extends NWActivity{

	static String recentCapsuleXpath = CreateCapsule.recentCapsuleXpath;
	static String publishingTabXpath = CreateCapsule.publishingTabXpath;
	
	static String editButtonXpath = "/html/body/div/div[2]/div/div/div/nav/ul/li[3]/a";
	
	public static void delete() {
		
		WebElement deleteButton = driver.findElement(By.cssSelector("button[ng-click='capsule.delete();']"));
		deleteButton.click();
	}
	
	public static void confirmDeletion(String capsuleName) {
		driver.get(getContentURL());
		CreateCapsule.openAgency();
		CreateCapsule.openClient();
		
		assertFalse(driver.getPageSource().contains(capsuleName));
	}
}
