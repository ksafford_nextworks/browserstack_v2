package com.nextworks.selenium_tests;

import static org.junit.Assert.assertEquals;

public class FakeLogin extends LoginAndLogout{

	public static final String FAKE_USERNAME = "LevTolstoy";
	public static final String FAKE_PASSWORD = "WrongPassword";
	
	public static void assertLoginDidNotHappen() {
		String currentUrl = driver.getCurrentUrl();
		assertEquals(currentUrl, PROTOCOL + "://" + SERVER_NAME+"login");
	}
}
