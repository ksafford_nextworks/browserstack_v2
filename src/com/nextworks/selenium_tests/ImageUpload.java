package com.nextworks.selenium_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ImageUpload extends NWActivity{

	static String title = "image1";
	static String description = "description1";
	static String imageToUpload = "/Users/ksafford/Desktop/Documents/test images/catfish.jpg";
	
	static String widgetName = "Image";
	static String categoryName = "Basic Image";
	
	
	public static void addImageWidget() {
		addWidget(widgetName, categoryName, title, description);
		
		WebElement browseButton = driver.findElement(By.cssSelector("input[ng-model='uploadedFile']"));
		browseButton.sendKeys(imageToUpload);
		
		
		/*
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply (WebDriver driver) {
				WebElement imageWrapper = driver.findElement(By.className("image-widget"));
				String styleAttribute = imageWrapper.getAttribute("style");
				if (!styleAttribute.equals("background-image: none;")) 
					return true;
					else
						return false;
				
			}
		});
		*/
		
		clickOkWidgetButton();
		
		
	}
	
	
}
