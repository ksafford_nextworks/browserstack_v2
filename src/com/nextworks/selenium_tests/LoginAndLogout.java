package com.nextworks.selenium_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;

public class LoginAndLogout extends NWActivity{
	
	public static void login(String username, String password) {
		WebElement usernameField = driver.findElement(By.cssSelector("input[data-ng-model='username']"));
		usernameField.clear();
		usernameField.sendKeys(username);
		
		WebElement passwordField = driver.findElement(By.cssSelector("input[data-ng-model='password']"));
		passwordField.clear();
		passwordField.sendKeys(password);
		passwordField.sendKeys(Keys.ENTER);
	}
	
	public static void verifyLogin() {
		String logoCssSelector = "a[ui-sref='app.landing']";
		waitForElement(logoCssSelector);
		
		String currentUrl = driver.getCurrentUrl();
		assertEquals(currentUrl, PROTOCOL + "://" + SERVER_NAME);
	}
	
	public static void logout() {
		WebElement username = driver.findElement(By.cssSelector("b[class='pull-right m-t-n-md caret']"));
		username.click();
		WebElement logout = driver.findElement(By.linkText("Logout"));
		logout.click();
	}
	
	public void verifyLogout() {
		WebElement username = driver.findElement(By.cssSelector("input[data-ng-model='username']"));
		assertTrue(username.isDisplayed());
		
		WebElement password = driver.findElement(By.cssSelector("input[data-ng-model='password']"));
		assertTrue(password.isDisplayed());
		
		String currentUrl = driver.getCurrentUrl();
		assertEquals(currentUrl, getContentURL()+"login");
	}
}
