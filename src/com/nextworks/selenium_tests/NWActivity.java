package com.nextworks.selenium_tests;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NWActivity {
	protected static final String SERVER_NAME = "portal-stg.contentcapsule.com/";
	protected static final String USER_NAME = "ksafford";
	protected static final String PROTOCOL = "http";
	protected static final String PASSWORD = "password";
	protected static WebDriver driver;
	
	public static final String BS_USERNAME = "qatester4";
	public static final String BS_ACCESS_KEY = "xmF3BWZ3pDZvEd4bfELo";
	public static final String URL = "http://" + BS_USERNAME + ":" + BS_ACCESS_KEY + "@hub.browserstack.com/wd/hub";
	
	static String jsScript = "$('.widget-list-item').simulateDragDrop({ dropTarget: $('.template-row:nth-child(2)>div>div>.template-region:nth-child(1)') });";
	
	static String deleteTextWidgetFromEditorButtonXpath = ".//*[@id='assign-widgets-to-regions']/div/div/div[2]/div[1]/div/div/div/div[1]/a[1]";
	static String okButtonXpath = "/html/body/div[3]/div/div/div/div[3]/button[1]"; //in add widget window
	static String cancelButtonXpath = "/html/body/div[3]/div/div/div/div[3]/button[2]"; // in same window
	static String lastAddedWidgetOnRightHandPanelXpath = "/html/body/div/div[3]/div/div/div/div[2]/div/div/div/div/div[1]/div[2]/ul/li/div";
	
	static String secondaryText1Tab = "div[data-page-template-id='secondary-text-1']";
	static String homeVideoFeature = "div[data-page-template-id='home-video-feature']";
	
	protected static void runWithBrowserstack(String testName) throws Exception {
		openBrowserstack(testName);
		LoginAndLogout.login(USER_NAME, PASSWORD);
	}
	
	protected static void openBrowserstack(String testName) throws Exception{
		setRemoteDriver(testName);
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		driver.get(getContentURL());
		driver.manage().window().maximize();
	}
	
	protected static void setRemoteDriver(String testName) throws Exception{
		DesiredCapabilities caps = new DesiredCapabilities();
	    caps.setCapability("browser", "Firefox");
	    caps.setCapability("browser_version", "36.0");
	    caps.setCapability("os", "Windows");
	    caps.setCapability("os_version", "8.1");
	    caps.setCapability("browserstack.debug", "true");
	    caps.setCapability("name", testName);
	    caps.setCapability("project", "NextworksTests");
	    caps.setCapability("resolution", "2048x1536");
	    
	    driver = new RemoteWebDriver(new URL(URL), caps);
	}
	
	protected static void runLocally() {
		setWebDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(getContentURL());
		driver.manage().window().maximize();
		
		LoginAndLogout.login(USER_NAME, PASSWORD);
	}
	
	protected static void setWebDriver() {
		driver = new FirefoxDriver();
	}
	
	protected static String getContentURL() {
		return PROTOCOL + "://" + SERVER_NAME;
	}
	
	protected static void waitForElementPresent(String xpath) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}
	
	protected static void waitForElement(String cssSelector) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
	}
	
	protected static void waitForTextToAppear(String id) {
		(new WebDriverWait(driver,15)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.findElement(By.id(id)).getText().length()!=0;
			}
		});
	}
	
	protected static void chooseOneColumnSimple() {
		WebElement oneColumnSimple = driver.findElement(By.cssSelector("div[data-page-template-id='1-column-simple']"));
		oneColumnSimple.click();
	}
	
	/*
	protected static void dragAndDropWidget(String widgetXpath, String targetXpath) {
		WebElement widget = driver.findElement(By.xpath(widgetXpath));
		WebElement target = driver.findElement(By.xpath(targetXpath));
		Actions builder = new Actions(driver);
		builder.dragAndDrop(widget, target).perform();
	}
	*/
	
	public static void dragAndDropWidget() {
		//WebElement textWidget = driver.findElement(By.name("ooyala1"));
		//WebElement source = driver.findElement(By.cssSelector("div[data-region-id='3']"));
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript(jsScript);
		
		//Actions builder = new Actions(driver);
		//builder.dragAndDrop(textWidget, source).build().perform();
		
		/*
		Actions builder = new Actions(driver);
		builder.moveToElement(textWidget).clickAndHold(textWidget).moveToElement(target).release(target).build().perform();
		*/
		
		/*
		Actions act = new Actions(driver);
		act.clickAndHold(textWidget);
		System.out.println(textWidget.getAttribute("class"));
		act.moveToElement(target);
		act.release(textWidget);
		act.build().perform();
		*/
		
		/*
		Actions builder = new Actions(driver);
		Action drag = builder.clickAndHold(textWidget).moveToElement(target).release(target).build();
		*/
		
		/*
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(textWidget, -300, 50).build().perform();
		*/
	}
	
	public static void deleteWidgetFromEditor(String widgetContainerXpath) {
		Actions action = new Actions(driver);
		WebElement widgetContainer = driver.findElement(By.xpath(widgetContainerXpath));
		action.moveToElement(widgetContainer).build().perform();
		
		WebElement deleteButton = driver.findElement(By.xpath(deleteTextWidgetFromEditorButtonXpath));
		deleteButton.click();
	}
	
	public static void assertWidgetDeletedFromEditor(String xpath) { //xpath of container + "//*"
		List<WebElement> allChildren = driver.findElements(By.xpath(xpath));
		//System.out.println(allChildren.size());
		assertEquals(allChildren.size(), 0);
	}

	protected static void chooseTabStyle(String tabStyle) {
		WebElement secondaryText1 = driver.findElement(By.cssSelector(tabStyle));
		secondaryText1.click();
	}
	
	protected static void addWidget(String widgetName, String widgetCategoryName, String widgetTitle, String widgetDescription) {
		WebElement widget = driver.findElement(By.xpath("//button[contains(., '"+widgetName+"')]"));
		widget.click();
		
		WebElement widgetCategory = driver.findElement(By.linkText(widgetCategoryName));
		widgetCategory.click();
		
		WebElement titleField = driver.findElement(By.id("title"));
		titleField.sendKeys(widgetTitle);
		
		WebElement descriptionField = driver.findElement(By.id("description"));
		descriptionField.sendKeys(widgetDescription);
		
		WebElement configureTab = driver.findElement(By.linkText("Configure"));
		configureTab.click();
	}
	
	public static void clickOkWidgetButton() {
		WebElement okButton = driver.findElement(By.linkText("OK"));
		okButton.click();
	}
}
