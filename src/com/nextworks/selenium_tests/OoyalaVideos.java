package com.nextworks.selenium_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;

public class OoyalaVideos extends YoutubeVideos{

	static String widgetName = YoutubeVideos.widgetName;
	static String categoryName = "Ooyala";
	
	static String[] regionIDs = {
		"1",
		"2",
		"3"
	};
	
	//static String testScript = "var myList=document.getElementsByClassName('widget-list-item');$(myList[0]).simulateDragDrop({ dropTarget: $('div[data-region-id="+regionID"]') });";
	
	static String[] ooyalaVideoNames= {
		"ooyala1",
		"ooyala2",
		"ooyala3"
	};
	
	static String[] ooyalaDescriptions = {
		"description1",
		"description2",
		"description3"
	};
	
	static String[] ooyalaVideoIDs = {
		"prdjA2czqzwhSWurK3qvUztR2QRjOL7_",
		"ppdjA2czpA9Usj2ngcVga8RClzRdt51L",
		"pndjA2czr9wrsy6Ma79wfe_09JzvReFS"
	};
	
	static String[] captions = {
		"caption1",
		"caption2",
		"caption3"
	};
	
	static String ooyalaPlayerID = "eaa976796c1b44558231024eb94144c0";
	
	public static void addOoyalaVideo() {
		
			addWidget(widgetName, categoryName, ooyalaVideoNames[0], ooyalaDescriptions[0]);
			
			WebElement videoIdField = driver.findElement(By.cssSelector("input[ng-model='config.videoId']"));
			videoIdField.sendKeys(ooyalaVideoIDs[0]);
			
			WebElement captionField = driver.findElement(By.cssSelector("input[ng-model='config.caption']"));
			captionField.sendKeys(captions[0]);
			
			WebElement playerIdField = driver.findElement(By.cssSelector("input[ng-model='config.playerId']"));
			playerIdField.sendKeys(ooyalaPlayerID);
			
			WebElement okButton = driver.findElement(By.linkText("OK"));
			okButton.click();
		
	}
	
	public static void assertOoyalaVideoAdded() {
			WebElement ooyalaWidget = driver.findElement(By.cssSelector("li[name='"+ooyalaVideoNames[0]+"']>a>i"));
			ooyalaWidget.click();
			
			WebElement titleField = driver.findElement(By.id("title"));
			String actualTitle = titleField.getAttribute("value");
			assertEquals(actualTitle, ooyalaVideoNames[0]);
			
			WebElement descriptionField = driver.findElement(By.id("description"));
			String actualDescription = descriptionField.getAttribute("value");
			assertEquals(actualDescription, ooyalaDescriptions[0]);
			
			WebElement ooyalaVideoIdField = driver.findElement(By.cssSelector("input[ng-model='config.videoId']"));
			String actualVideoId = ooyalaVideoIdField.getAttribute("value");
			assertEquals(actualVideoId, ooyalaVideoIDs[0]);
			
			WebElement captionField = driver.findElement(By.cssSelector("input[ng-model='config.caption']"));
			String actualCaption = captionField.getAttribute("value");
			assertEquals(actualCaption, captions[0]);
			
			WebElement playerIdField = driver.findElement(By.cssSelector("input[ng-model='config.playerId']"));
			String actualPlayerId = playerIdField.getAttribute("value");
			assertEquals(actualPlayerId, ooyalaPlayerID);
			
			WebElement cancelButton = driver.findElement(By.linkText("Cancel"));
			cancelButton.click();
		
	}

	public static void dragAndDropOoyala() {
		for (int i=0; i<ooyalaVideoNames.length; i++) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("$('li[name="+ooyalaVideoNames[i]+"]').simulateDragDrop({ dropTarget: $('div[data-region-id="+regionIDs[i]+"]') });");
		}
	}
}
