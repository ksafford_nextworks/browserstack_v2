package com.nextworks.selenium_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PdfFile extends NWActivity{

	static String widgetName = "Files";
	static String categoryName = "PDF Viewer";
	static String title = "PDF File";
	static String description = "PDF file description";
	
	static String pathToFileToUpload = "/Users/ksafford/Desktop/Documents/test images/i-765instr.pdf";
	
	public static void addPdfFile() {
		addWidget(widgetName, categoryName, title, description);
		
		WebElement uploadFileButton = driver.findElement(By.cssSelector("input[ng-model='uploadedFile']"));
		uploadFileButton.sendKeys();
		
		WebElement okButton = driver.findElement(By.linkText("OK"));
		okButton.click();
	}
}
