package com.nextworks.selenium_tests;

import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.junit.runner.Description;
import org.junit.rules.TestWatcher;

public class SendEmail extends TestWatcher{

	static Date now = new Date();
	static String currentTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(now);
	
	Session mailSession;

	@Override
	public void failed (Throwable e, Description description)  {
		try {
			email(description.getMethodName());
		} catch (AddressException e1) {
			e1.printStackTrace();
		} catch (MessagingException e1) {
			e1.printStackTrace();
		}

    }
	
	public static void email(String failedClass) throws AddressException, MessagingException {
		SendEmail javaEmail = new SendEmail();
        javaEmail.setMailServerProperties();
        javaEmail.draftEmailMessage(failedClass);
        javaEmail.sendEmail(failedClass);
	}

    private void setMailServerProperties()
    {
        Properties emailProperties = System.getProperties();
        emailProperties.put("mail.smtp.port", "587");
        emailProperties.put("mail.smtp.auth", "true");
        emailProperties.put("mail.smtp.starttls.enable", "true");
        mailSession = Session.getDefaultInstance(emailProperties, null);
    }

    private MimeMessage draftEmailMessage(String failedClass) throws AddressException, MessagingException
    {
        String[] toEmails = { "ksafford@nextworks.com" };
        String emailSubject = "Test failure";
        String emailBody = "This test (" + failedClass + ") has failed. "+currentTime;
        MimeMessage emailMessage = new MimeMessage(mailSession);
     
        for (int i = 0; i<toEmails.length; i++)
        {
            emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
        }
        emailMessage.setSubject(emailSubject);
        emailMessage.setText(emailBody);
        return emailMessage;
    }

    private void sendEmail(String failedClass) throws AddressException, MessagingException
    {
        
        String fromUser = "nextworksinga@gmail.com";
        String fromUserEmailPassword = "ebolafree";

        String emailHost = "smtp.gmail.com";
        Transport transport = mailSession.getTransport("smtp");
        transport.connect(emailHost, fromUser, fromUserEmailPassword);
        
        MimeMessage emailMessage = draftEmailMessage(failedClass);
        
        transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
        transport.close();
    }
}
