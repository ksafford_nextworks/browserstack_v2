package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

public class TestAddTabs extends AddTabs{

	static String testName = "AddTabs";
	
	@BeforeClass
	public static void setUp() throws Exception {
		runWithBrowserstack(testName);
		//runLocally();
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void TestAddTabs_NW() {
		CreateCapsule.createNewCapsule();
		addNewTab();
		assertTabsAdded();
		for (int i=0; i<newTabNames.length; i++) {
			renameTabs(defaultTabNames[i],tabsNamesXpathes[i], inputFieldsXpathes[i], newTabNames[i], okButtonsXpathes[i]);
		}
		for (int i=0; i<newTabNames.length; i++) {
			assertRename(tabsNamesXpathes[i], newTabNames[i]);
		}
		DeleteCapsule.delete();
	}
	
	@AfterClass
	public static void endTheTest() {
		driver.quit();
	}
}
