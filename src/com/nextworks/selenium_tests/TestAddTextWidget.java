package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TestAddTextWidget extends AddTextWidget{

	static String testName = "AddTextWidget";
	
	@BeforeClass
	public static void setUp() throws Exception{
		//runWithBrowserstack(testName);
		runLocally();
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void AddTextWidget_NW() throws InterruptedException{
		CreateCapsule.createNewCapsule();
		
		addTextWidget();
		assertWidgetIsAdded();
		
		chooseTabStyle(secondaryText1Tab);
		dragAndDropWidget();
		//assertTextDragged();
		
		//deleteWidgetFromEditor(textInTheEditorContainerXpath);
		//assertWidgetDeletedFromEditor(xpathForAssertWidgetDeletedFromEditorMethod);
		//assertTextIsNoLongerPresent();
		
		DeleteCapsule.delete();
	}
	
	@AfterClass
	public static void endTheTest() {
		//driver.quit();
	}
}
