package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

public class TestCreateCapsule extends CreateCapsule {

	static String testName = "CreateCapsule";
	
	@BeforeClass
	public static void setUp() throws Exception{
		runWithBrowserstack(testName);
		//runLocally();
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void TestCreateCapsule_NW() {
		createNewCapsule();
		renameCapsule();
		addDescription();

		capsuleName = getCapsuleName();
		capsuleID = getCapsuleId();
		
		saveCapsule();
		checkCapsuleWasSaved(capsuleID, capsuleName);
		
		DeleteCapsule.delete();
		DeleteCapsule.confirmDeletion(capsuleName);
	}
	
	@AfterClass
	public static void endTheTest() {
		driver.quit();
	}
}
