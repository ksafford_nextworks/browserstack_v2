package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

public class TestFakeLogin extends FakeLogin {

	static String testName = "FakeLogin";
	
	@BeforeClass
	public static void setUp() throws Exception{
		openBrowserstack(testName);
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void FakeLogin_NW() {
		login(FAKE_USERNAME, FAKE_PASSWORD);
		assertLoginDidNotHappen();
		
		login(USER_NAME, FAKE_PASSWORD);
		assertLoginDidNotHappen();
		
		login(FAKE_USERNAME, PASSWORD);
		assertLoginDidNotHappen();
	}
	
	@AfterClass
	public static void endOfTest() {
		driver.quit();
	}
	
}
