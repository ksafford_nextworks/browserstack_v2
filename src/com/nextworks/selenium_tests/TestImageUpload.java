package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

public class TestImageUpload extends ImageUpload{

	static String testName = "ImageUpload";
	
	@BeforeClass
	public static void setUp() throws Exception{
		//runWithBrowserstack(testName);
		runLocally();
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void ImageUpload_NW() {
		CreateCapsule.createNewCapsule();
		addImageWidget();
	}
	
	@AfterClass
	public static void endTheTest() {
		//DeleteCapsule.delete();
		//driver.quit();
	}
}
