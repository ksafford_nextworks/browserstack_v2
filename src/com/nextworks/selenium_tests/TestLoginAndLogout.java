package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

public class TestLoginAndLogout extends LoginAndLogout{

	static String testName = "LoginAndLogout";
	
	@BeforeClass
	public static void setUp() throws Exception{
		openBrowserstack(testName);
		//runLocally();
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void TestLoginAndLogout_NW() {
		login(USER_NAME, PASSWORD);
		verifyLogin();
		logout();
		verifyLogout();
	}
	
	@AfterClass
	public static void endTheTest() {
		driver.quit();
	}
}
