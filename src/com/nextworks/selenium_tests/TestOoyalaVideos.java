package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TestOoyalaVideos extends OoyalaVideos{

	static String testName = "OoyalaVideos";
	
	@BeforeClass
	public static void setUp() throws Exception {
		//runWithBrowserstack(testName);
		runLocally();
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void OoyalaVideos_NW() {
		CreateCapsule.createNewCapsule();
		
		addOoyalaVideo();
		assertOoyalaVideoAdded();
		
		//chooseTabStyle(homeVideoFeature);
		//dragAndDropOoyala();
		DeleteCapsule.delete();
	}
	
	@AfterClass
	public static void endTheTest() {
		//driver.quit();
	}
	
}
