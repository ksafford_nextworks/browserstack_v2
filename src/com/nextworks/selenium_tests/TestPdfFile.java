package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

public class TestPdfFile extends PdfFile{

	static String testName = "AddPdfFile";
	
	@BeforeClass
	public static void setUp() throws Exception {
		//runWithBrowserstack(testName);
		runLocally();
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void AddPdfFile_NW() {
		CreateCapsule.createNewCapsule();
		addPdfFile();
	}
	
	@AfterClass
	public static void endTheTest() {
		//driver.quit();
	}
}
