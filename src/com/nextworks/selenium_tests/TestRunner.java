package com.nextworks.selenium_tests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner extends junit.textui.TestRunner{

	public static void main(String[] args) {
	      Result result = JUnitCore.runClasses(NWTestSuite.class);
	      for (Failure failure : result.getFailures()) {
	         System.out.println(failure.toString());
	      }
	      
	     if (result.wasSuccessful()) {
	    	 System.out.println("Done");
	      }
	   }
	
}
