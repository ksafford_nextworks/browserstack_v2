package com.nextworks.selenium_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

public class TestYoutubeVideos extends YoutubeVideos {

	static String testName = "YoutubeVideos";
	
	@BeforeClass
	public static void setUp() throws Exception {
		//runWithBrowserstack(testName);
		runLocally();
	}
	
	@Rule
	public SendEmail send = new SendEmail();
	
	@Test
	public void YoutubeVideos_NW() {
		CreateCapsule.createNewCapsule();
		addYoutubeVideo();
		assertVideoAdded();
		
		//chooseTabStyle(homeVideoFeature);
		//dragAndDropWidget();
		//assertVideoDragged(); need regionId to embed video
		
		//deleteWidgetFromEditor(draggedVideoXpath);
		//assertWidgetDeletedFromEditor(xpathForAssertWidgetDeletedFromEditorMethod);
		
		DeleteCapsule.delete();
	}
	
	@AfterClass
	public static void endTheTest() {
		//driver.quit();
	}
}
