package com.nextworks.selenium_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;

public class YoutubeVideos extends NWActivity{

	static String widgetName = "Video";
	static String categoryName = "YouTube";
	static String youtubeVideoUrlFieldCssSelector = "input[ng-model='config.videoUrl']";
	static String draggedVideoXpath = "/html/body/div/div[3]/div/div/div/div[1]/div[2]/div/div/div/assign-widgets-to-regions/div/div/div/div[2]/div[1]/div/div/div/div/div/div/div/div/img";
	static String xpathForAssertWidgetDeletedFromEditorMethod = ".//*[@id='assign-widgets-to-regions']/div/div/div[2]/div[1]/div/div//*";
	
	static String title = "Youtube Video";
	static String description = "Description";
	
	static String youtubeURL = "https://www.youtube.com/watch?v=JrQkgLLL9XQ";
	static String youtubeVideoId = "JrQkgLLL9XQ";
	static String expectedSrcAttribute = "http://img.youtube.com/vi/"+youtubeVideoId+"/0.jpg";
	
	public static void addYoutubeVideo() {
		addWidget(widgetName, categoryName, title, description);
		
		WebElement youtubeVideoUrlField = driver.findElement(By.cssSelector(youtubeVideoUrlFieldCssSelector));
		youtubeVideoUrlField.sendKeys(youtubeURL);
		
		WebElement okButton = driver.findElement(By.linkText("OK"));
		okButton.click();
	}
	
	public static void assertVideoAdded() {
		WebElement widgetName = driver.findElement(By.name(title));
		String addedVideoWidgetName = widgetName.getText();
		assertEquals(addedVideoWidgetName, title);
		
		WebElement widgetSettings = driver.findElement(By.cssSelector("li[name='"+title+"']>a>i"));
		widgetSettings.click();
		
		WebElement titleField = driver.findElement(By.id("title"));
		String actualTitle = titleField.getAttribute("value");
		assertEquals(actualTitle, title);
		
		WebElement descriptionField = driver.findElement(By.id("description"));
		String actualDescription = descriptionField.getAttribute("value");
		assertEquals(actualDescription, description);
		
		WebElement configureTab = driver.findElement(By.linkText("Configure"));
		configureTab.click();
		
		WebElement youtubeVideoUrlField = driver.findElement(By.cssSelector(youtubeVideoUrlFieldCssSelector));
		String actualYoutubeUrl = youtubeVideoUrlField.getAttribute("value");
		assertEquals(actualYoutubeUrl, youtubeURL);
		
		WebElement cancelButton = driver.findElement(By.linkText("Cancel"));
		cancelButton.click();
	}

	public static void assertVideoDragged() {
		//WebElement draggedVideo = driver.findElement(By.cssSelector(cssSelectorForDraggedVideo));
		//String srcAttribute = draggedVideo.getAttribute("src");
		//assertEquals(srcAttribute, expectedSrcAttribute);
		
	}
}
